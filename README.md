Some useful docker images, now contains:
* jdk
* mysql
* nginx
* tomcat
* jenkins
* php-fpm
* [zentao](https://git.oschina.net/gavincook/zentao-docker)

...

more images are adding. 

_Comment:_ take a look for README.md under each image folder for details.